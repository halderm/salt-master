# README #

Project to install a Debian Jessie development environment using
masterless saltstack, vagrant and virtualbox.

### Restrictions ###

Due to a bug in vagrant (issue 5973), a workaround using "shell"
provisioning had to be done.
Downgrading to vagrant 1.7.2 and virtualbox 4.3 is not always possible.

### What is this repository for? ###

* create a virtualbox debian vm with vagrant
* install development packages with salt
* creates user halderm with password halderm
* clones https://gitlab.com/halderm/config.git and installs
  configuration for
    * git
    * i3
    * tmux
    * vim
    * zsh
    * vim plugins and ycm will be installed on first start of vim

### How do I get set up? ###

* install necessary packages
  - install vagrant
  - optional: install vbguest plugin for updated guest extensions
  - install virtualbox

```bash
sudo apt-get install vagrant virtualbox
```

```bash
vagrant plugin install vagrant-vbguest
```

* clone this repository

```bash
git clone https://gitlab.com/halderm/salt-master.git
```

* configure users and groups in pillar

```bash
vim salt-master/masterless/salt/roots/pillar/users.sls
vim salt-master/masterless/salt/roots/pillar/groups.sls
```

* run vagrant

```bash
cd salt-master/masterless
vagrant up
```

* connect to vagrant box

```bash
vagrant ssh
vagrant ssh -- -l halderm
ssh -p 2222 halderm@localhost
```

* use salt inside machine

```bash
sudo salt-call state.highstate
```

* cleanup vagrant box

```bash
vagrant destroy default
vagrant box remove debian/jessie64
```

### Contributors ###

* Martin Halder
* Eric Keller
* Dirk Honisch
