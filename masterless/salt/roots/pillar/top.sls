base:
  '*':
    {% if "usecase" in grains and grains['usecase'] == 'server' %}
    - users_server
    {% else %}
    - users
    - groups
    {% endif%}
