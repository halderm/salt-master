users:
  halderm:
    fullname: halderm
    shell: /bin/zsh
    {% if grains['os'] == 'MacOS' %}
    home: /Users/halderm
    {% else %}
    home: /home/halderm
    {% endif %}
    groups:
      - admin
    {% if not grains['os'] == 'MacOS' %}
      - sudo
    {% endif %}
    password: $6$rl2lpVeH$C/Xaw6pU0jyosvEp3P89YzDH0xB5Ws6Zae4UYp/PhdLOv8jz6a7DrUGDRxyG/Ge2qFEwwr0eCCv0QXtRhKxWZ/
    enforce_password: False
    key.pub: True
