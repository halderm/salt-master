users:
  buildr:
    fullname: buildr
    shell: /bin/zsh
    {% if grains['os'] == 'MacOS' %}
    home: /Users/buildr
    {% else %}
    home: /home/buildr
    {% endif %}
    {% if not grains['os'] == 'MacOS' %}
    groups:
      - sudo
    {% endif %}
    password: $6$0QVyPSlU$Qu9FcXmBA/DjRXnSGDtSoSxJcTHje4gKFUZ9vQaBdDW9k4I/uFXdxpnBfRCJYLWpIp1g09eifs3rCcWqif.qe0
    enforce_password: False
    key.pub: True
