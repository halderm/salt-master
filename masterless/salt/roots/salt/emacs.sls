{% for user, args in pillar['users'].iteritems() %}
emacs:
  pkg:
    - installed

prelude:
  git.latest:
    - name: https://github.com/bbatsov/prelude.git
    - rev: master
    - target: {{ salt['user.info'](user).get('home') }}/src/prelude
    - user: {{ user }}
    - require:
      - pkg: emacs

evil:
  git.latest:
    - name: https://gitlab.com/halderm/evil.git
    - rev: master
    - target: {{ salt['user.info'](user).get('home') }}/src/evil
    - user: {{ user }}
    - require:
      - pkg: emacs
      - git: prelude

async:
  git.latest:
    - name: https://github.com/jwiegley/emacs-async.git
    - rev: master
    - target: {{ salt['user.info'](user).get('home') }}/src/async
    - user: {{ user }}
    - require:
      - pkg: emacs
      - git: prelude

helm:
  git.latest:
    - name: https://github.com/emacs-helm/helm.git
    - rev: master
    - target: {{ salt['user.info'](user).get('home') }}/src/helm
    - user: {{ user }}
    - require:
      - pkg: emacs
      - git: prelude
      - git: async
  cmd.wait:
      - name: make
      - cwd: {{ salt['user.info'](user).get('home') }}/src/helm
      - watch:
        - git: helm

deft:
  git.latest:
    - name: https://github.com/jrblevin/deft.git
    - rev: master
    - target: {{ salt['user.info'](user).get('home') }}/src/deft
    - user: {{ user }}
    - require:
      - pkg: emacs
      - git: prelude

undo-tree:
  git.latest:
    - name: https://github.com/emacsmirror/undo-tree.git
    - rev: master
    - target: {{ salt['user.info'](user).get('home') }}/src/undo-tree
    - user: {{ user }}
    - require:
      - pkg: emacs
      - git: prelude

{{ salt['user.info'](user).get('home') }}/.emacs.d:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/src/prelude
    - user: {{ user }}
    - require:
      - git: config
      - pkg: tmux

org-mode:
  pkg:
    - installed

{{ salt['user.info'](user).get('home') }}/src/prelude/prelude-modules.el:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/emacs/prelude-modules.el
    - user: {{ user }}
    - force: True
    - require:
      - git: config
      - pkg: emacs
      - git: prelude

{{ salt['user.info'](user).get('home') }}/src/prelude/personal/custom.el:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/emacs/custom.el
    - user: {{ user }}
    - force: True
    - require:
      - git: config
      - pkg: emacs
      - git: prelude

{{ salt['user.info'](user).get('home') }}/src/prelude/modules/prelude-key-chord.el:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/emacs/modules/prelude-key-chord.el
    - user: {{ user }}
    - force: True
    - require:
      - git: config
      - pkg: emacs
      - git: prelude
{% endfor %}
