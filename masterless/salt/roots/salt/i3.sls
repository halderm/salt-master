{% for user, args in pillar['users'].iteritems() %}
i3:
  pkg:
    - installed
    - require:
      - pkg: xorg

{{ salt['user.info'](user).get('home') }}/.i3:
  file.symlink:
    - target: {{ salt['user.info'](user).get('home') }}/config/i3
    - user: {{ user }}
    - require:
      - git: config

{{ salt['user.info'](user).get('home') }}/images/background.jpg:
  file.managed:
    - source: salt://images/background.jpg
    - user: {{ user }}
    - makedirs: True
    - require:
      - pkg: i3
{% endfor %}
