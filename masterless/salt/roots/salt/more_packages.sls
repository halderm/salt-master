xorg:
  pkg:
    - installed

feh:
  pkg:
    - installed

{% if grains['os'] == 'Ubuntu' %}
rdesktop:
  pkg:
    - installed

scrot:
  pkg:
    - installed

network-manager-gnome:
  pkg:
    - installed

xfce4-power-manager:
  pkg:
    - installed

ack-grep:
  pkg:
    - installed

autojump:
  pkg:
    - installed

pavucontrol:
  pkg:
    - installed
{% endif %}
