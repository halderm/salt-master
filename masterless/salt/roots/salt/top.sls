base:
  '*':
    - base_packages
    - git
    - vim
    - config
    {% if "usecase" in grains and grains['usecase'] == 'server' %}
    - users
    {% elif "usecase" in grains and grains['usecase'] == 'centrify' %}
    - more_packages
    - emacs
    - i3
    {% elif grains['os'] == 'MacOS' %}
    - users
    - groups
    - emacs
    {% else %}
    - more_packages
    - emacs
    - i3
    {% endif%}
