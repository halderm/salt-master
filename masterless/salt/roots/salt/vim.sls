{% if grains['os'] == 'MacOS' %}
vim:
  pkg:
    - installed
    - require:
      - pkg: python

macvim:
  pkg:
    - installed
    - require:
      - pkg: python
{% else %}
vim-nox:
  pkg:
    - installed
    - require:
      - pkg: python
{% endif %}

{% if not grains['os'] == 'MacOS' %}
python-dev:
  pkg:
    - installed
    - require:
      - pkg: python
{% endif %}

cmake:
  pkg:
    - installed
